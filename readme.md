# Akka Based Spray client


## Highlights

- spray json - `using spray json protocol`

- Jso4s - `using json4s`

## To run

`sbt run`
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list

Multiple main classes detected, select one to run:

 [1] MovieSpray
 [2] MovieWithJson4s

Enter number: 1

[info] Running MovieSpray 
[INFO] [03/22/2017 12:01:31.612] [run-main-0] [MovieSpray$(akka://simple-spray-client)] Requesting the Guardians Movie...


`Guardians of the Galaxy
The Hitchhiker's Guide to the Galaxy
Galaxy Quest
The Hitch Hikers Guide to the Galaxy
Galaxy of Terror
Super Mario Galaxy
Galaxy Express 999
Marvel's Guardians of the Galaxy
Power Rangers Lost Galaxy
The Tatami Galaxy`

[success] Total time: 4 s, completed Mar 22, 2017 12:01:32 PM

`sbt run`
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list

Multiple main classes detected, select one to run:

 [1] MovieSpray
 [2] MovieWithJson4s

Enter number: 2

[info] Running MovieWithJson4s 

TotalResult(162)
[success] Total time: 2 s, completed Mar 22, 2017 12:01:39 PM



## Reference

### SPRAY
   
- [spray client](http://spray.io/documentation/1.2.4/spray-client/)
- [spray json](https://github.com/spray/spray-json)

         
### JSON4S
   
- [json4s](https://github.com/json4s/json4s) 