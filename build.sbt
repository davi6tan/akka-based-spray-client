name := "sprayjson4"

version := "1.0"

scalaVersion := "2.11.7"

parallelExecution in Test := false

resolvers ++= Seq("RoundEights" at "http://maven.spikemark.net/roundeights")

libraryDependencies ++= {
  val logbackVersion = "1.1.3"
  val akkaVersion = "2.3.9"
  val sprayVersion = "1.3.3"

  Seq(
    "org.slf4j" % "slf4j-api" % "1.7.7",
    "ch.qos.logback" % "logback-core" % logbackVersion,
    "ch.qos.logback" % "logback-classic" % logbackVersion,
    "io.spray" %% "spray-can" % sprayVersion,
    "io.spray" %% "spray-routing" % sprayVersion,
    "io.spray" %% "spray-caching" % sprayVersion,
    "io.spray" %% "spray-client" % sprayVersion,
    "io.spray" %% "spray-json" % "1.3.1",
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "com.gettyimages" %% "spray-swagger" % "0.5.1",
    "com.jsuereth" %% "scala-arm" % "1.4"
  )
}