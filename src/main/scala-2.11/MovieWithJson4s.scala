/**
  * Created by davidtan on 3/22/17.
  */
import org.json4s._
import org.json4s.native.JsonMethods._

import scala.io._

object MovieWithJson4s {

  implicit val formats = DefaultFormats
  case class TotalResult(n:String)

  def fetchTotalResultFromUrl(url:String):TotalResult= {
    val response = Source.fromURL(url).mkString
    val jsonResponse = parse(response)
    extractTotalResult(jsonResponse)
  }

  def extractTotalResult(obj:JValue):TotalResult= {
    val transformedObject = obj.transformField {
      case ("totalResults", totalresult) => ("n", totalresult)
    }
    transformedObject.extract[TotalResult]
  }

  def main(args:Array[String]) {
    val url = "http://www.omdbapi.com/?s=Galaxy"
    val totalResult = fetchTotalResultFromUrl(url)

    println()
    println(totalResult)

  }

}
/*
TotalResult(162)

 */