/**
  * Created by davidtan on 3/22/17.
  */
import scala.util.{Failure, Success}
import scala.concurrent.duration._
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.event.Logging
import akka.io.IO
import spray.json.{DefaultJsonProtocol, JsonFormat}
import spray.can.Http
import spray.httpx.SprayJsonSupport
import spray.client.pipelining._
import spray.util._

case class SearchResult[T](Response: String, Search:List[T])
case class Movie(Poster:String,Title:String,Type:String,Year:String,imdbID:String)

object SearchJsonProtocol extends DefaultJsonProtocol {
  implicit val anyFormat = jsonFormat5(Movie)
  implicit def searchResultFormat[T :JsonFormat] = jsonFormat2(SearchResult.apply[T])

}

object MovieSpray extends App {
  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("simple-spray-client")
  import system.dispatcher // execution context for futures below
  val log = Logging(system, getClass)

  log.info("Requesting the Guardians Movie...")

  import SearchJsonProtocol._
  import SprayJsonSupport._
  val pipeline = sendReceive ~> unmarshal[SearchResult[Movie]]

  val responseFuture = pipeline {
    Get("http://www.omdbapi.com/?s=Galaxy")
  }

  responseFuture onComplete {
    case Success(SearchResult(a,xs)) =>
      for(z<-xs)println(z.Title)
      shutdown()

    case Success(somethingUnexpected) =>
      log.warning("The Search  call was successful but returned something unexpected: '{}'.", somethingUnexpected)
      shutdown()

    case Failure(error) =>
      log.error(error, "Couldn't get any search")
      shutdown()
  }

  def shutdown(): Unit = {
    IO(Http).ask(Http.CloseAll)(1.second).await
    system.shutdown()
  }
}
/*
[INFO] [03/22/2017 06:55:38.791] [main] [MovieSpray$(akka://simple-spray-client)] Requesting the Guardians Movie...
Guardians of the Galaxy
The Hitchhiker's Guide to the Galaxy
Galaxy Quest
The Hitch Hikers Guide to the Galaxy
Galaxy of Terror
Super Mario Galaxy
Galaxy Express 999
Marvel's Guardians of the Galaxy
Power Rangers Lost Galaxy
The Tatami Galaxy
 */